﻿# API REFERENCES

_A practical guide to use this simple API._

## Introduction:

This API got only few endpoints, each endpoints may have is particularity.
This API references will explain how to use each endpoints and describe their action, and parameters.

All endpoints support their singular version to avoid user error, for example you can use the endpoint Matches with ```localhost/api/v1/match``` instead of ```localhost/api/v1/matches```.
We will only use the plural routes to refer a endpoint in this API.

## Matches endpoint:
This endpoint will simply throw a list of all elements present in the database without any special formatting.

use as follow:
```
http://localhost/api/v1/matches
```

You can specify the format of response in parameter, it will by default send JSON:

```
http://localhost/api/v1/matches?format=json
```
To have further informations about _format_ parameters please refer to the format section.

## Seasons endpoint:

This endpoint will return a list of Seasons containing an array of associated Leagues.
```
{"data": 
  [
    {
      "season": "201516",
      "league": [ SP1, SP2 ...]
    }, {...}
  ]
}
```
use as follow:
```
http://localhost/api/v1/seasons
```
You can specify the format of response in parameter, it will by default send JSON:

```
http://localhost/api/v1/seasons?format=json
```
To have further informations about _format_ parameters please refer to the format section.

## Leagues endpoint:

This endpoint will return a list of Leagues containing an array of associated Seasons.
```
{"data":
  [
    {
      "leagues": "liga",
      "season": [ "seasonspair", "seasonpair", ...]
    }, {...}
  ]
}
```
use as follow:

```
http://localhost/api/v1/leagues
```
You can specify the format of response in parameter, it will by default send JSON:

```
http://localhost/api/v1/leagues?format=json
```
To have further informations about _format_ parameters please refer to the format section.

## Results endpoint:

Results endpoint returns a list of game with it's relative information.
It has a lot available parameters so you can find what you need.

```
{"data": 
  [
    {
      "date": "dd-mm-yyyy",
      "games": "HomeTeamName - AwayTeamName",
      "league": "LIGA",
      "results": "x - x",
      "season": "seasonpair",
      "winner": "Drawn"
    }, {...}
  ]
}
```

use as follow:

```
http://localhost/api/v1/results

http://localhost/api/v1/results?home=barcelona&season=201617
```

#### parameters

This endpoint can take a lot of parameters to filter the available results,

the parameters are combinable, and their values should always be entered in downcase.

* __id__ - return element with matching id
```
http://localhost/api/v1/results?id=5
```

* __league__ - return elements with matching league
```
http://localhost/api/v1/results?league=sp1
```

* __season__ - return elements with matching season pair
```
http://localhost/api/v1/results?season=201516
```

* __team__ - this parameter will return any games with the given team name.
```
http://localhost/api/v1/results?team=barcelona
```

* __home__ - this parameter will return any games with the given team name as home team.
```
http://localhost/api/v1/results?home=barcelona
```

* __away__ - this parameter will return any games with the given team name as away team.
```
http://localhost/api/v1/results?away=west ham
```

* __ftr__ - Full Time Result, this is the final result of a game.
it can contain the following letter _h, a, d_, respectively for _home, away, drawn_.
```
http://localhost/api/v1/results?ftr=h
```

* __htr__ - Half Time Result, this is the result of a game during half time.
as **ftr** paramets it can contain the follow letter _h, a, d_.
```
http://localhost/api/v1/results?htr=a
```

* __date__ - take a formated string to return every games that happen at the given date,
the date need to be as follow: _day-month-year_
```
http://localhost/api/v1/results?date=dd-mm-yyyy
```

* __fthg__ - Full Time Home Goals, find games by numbers of goals of the home team at the end of the game.
```
http://localhost/api/v1/results?fthg=6
```

* __ftag__ - Full Time Away Goals, find games by numbers of goals of the away team at the end of the game.
```
http://localhost/api/v1/results?ftag=7
```

* __hthg__ - Half Time Home Goals, same as **fthg** parameters, but for half times.
```
http://localhost/api/v1/results?hthg=4
```

* __htag__ - Half Time Away Goals, same as **ftag** parameters, but for half times.
```
http://localhost/api/v1/results?htag=4
```

## Format parameters

the format parameters let the user choose the response format of the API.

Currently only two formats are supported:

* __JSON__
```
http://localhost/api/v1/matches?format=json
http://localhost/api/v1/seasons?format=json
http://localhost/api/v1/leagues?format=json
http://localhost/api/v1/results?format=json
```

* __Protocol Buffers__

```
http://localhost/api/v1/matches?format=protobuf
http://localhost/api/v1/seasons?format=protobuf
http://localhost/api/v1/leagues?format=protobuf
http://localhost/api/v1/results?format=protobuf
```

As the http content-type header is set on application/octet-stream,
some browser may force you to download the datas when you query it.

For a more convenient try of this feature I invite you to use curl
```
$ curl http://localhost/api/v1/leagues?format=protobuf
```
