defmodule Derivco.SoccerTest do
  use Derivco.DataCase

  alias Derivco.Soccer
  alias Derivco.Soccer.Match

  describe "list_matches/0" do
    test "list_matches/0 returns all matches" do
      matches = Soccer.list_matches
      assert matches != nil || []
    end

    test "list_matches/0 returns is a list" do
      matches = Soccer.list_matches
      assert is_list(matches) == true
    end

    test "list_matches/0 returns element in his list" do
      matches = Soccer.list_matches
      assert length(matches) >= 1
    end

    test "list_matches/0 return contains all keys" do
      keys = [
        :id, :season, :league, :home,
        :away, :date, :ftr, :htr,
        :fthg, :ftag, :hthg, :htag
      ]

      match = List.first(Soccer.list_matches)

      ret = Enum.all?(keys, fn(k) ->
        Map.has_key?(match, k)
      end)

      assert ret == true
    end
  end

  describe "list_leagues/0" do
    test "list_leagues/0 returns all leagues" do
      leagues = Soccer.list_leagues
      assert leagues != nil || []
    end

    test "list_leagues/0 returns is a list" do
      leagues = Soccer.list_leagues
      assert is_list(leagues) == true
    end

    test "list_leagues/0 returns element in his list" do
      leagues = Soccer.list_leagues
      assert length(leagues) >= 1
    end

    test "list_leagues/0 return contains all keys" do
      keys = [:league, :season]

      league = List.first(Soccer.list_leagues)

      ret = Enum.all?(keys, fn(k) ->
        Map.has_key?(league, k)
      end)

      assert ret == true
    end
  end

  describe "list_seasons/0" do
    test "list_seasons/0 returns all seasons" do
      seasons = Soccer.list_seasons
      assert seasons != nil || []
    end

    test "list_seasons/0 returns is a list" do
      seasons = Soccer.list_seasons
      assert is_list(seasons) == true
    end

    test "list_seasons/0 returns element in his list" do
      seasons = Soccer.list_seasons
      assert length(seasons) >= 1
    end

    test "list_seasons/0 return contains all keys" do
      keys = [:league, :season]

      season = List.first(Soccer.list_seasons)

      ret = Enum.all?(keys, fn(k) ->
        Map.has_key?(season, k)
      end)

      assert ret == true
    end
  end

  describe "list_results/2" do
    test "list_results/2 returns all matches" do
      p1 = []
      p2 = [{"id", "4"}]
      p3 = [{"team", "fromage"}]
      p4 = [{"date", "17"}]
      p5 = [{"season", "17"}]
      p6 = [{"seasons", "17"}]

      assert Soccer.list_results(Match, p1) != nil || []
      assert Soccer.list_results(Match, p2) != nil || []
      assert Soccer.list_results(Match, p3) == nil || []
      assert Soccer.list_results(Match, p4) == nil || []
      assert Soccer.list_results(Match, p5) == nil || []
      assert Soccer.list_results(Match, p5) == Soccer.list_results(Match, p6)
    end

    test "list_results/2 returns is a list" do
      results = Soccer.list_results(Match, [])
      assert is_list(results) == true
    end

    test "list_results/2 return contains all keys" do
      keys = [
        :id, :season, :league, :home,
        :away, :date, :ftr, :htr,
        :fthg, :ftag, :hthg, :htag
      ]

      result = List.first(Soccer.list_results(Match, []))

      ret = Enum.all?(keys, fn(k) ->
        Map.has_key?(result, k)
      end)

      assert ret == true
    end
  end

end
