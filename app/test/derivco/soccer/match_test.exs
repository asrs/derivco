defmodule Derivco.MatchTest do
  use Derivco.DataCase

  alias Derivco.Soccer.Match

  # doctest Soccer

  test "test changeset with valid parameters" do
    value = %{
      id: 3000,
      league: "PS1",
      season: "201819",
      date: "15-04-2018",
      home: "home",
      away: "away",
      fthg: 0,
      ftag: 0,
      ftr: "d",
      hthg: 0,
      htag: 0,
      htr: "d"
    }
    assert Match.changeset(%Match{}, value).valid? == true
  end

  test "test changeset with missing parameters" do
    value = %{
      id: 3000,
      league: "PS1",
      date: "15-04-2018",
      home: "home",
      away: "away",
      fthg: 0,
      ftag: 0,
      ftr: "d",
      hthg: 0,
      htag: 0,
      htr: "d"
    }
    assert Match.changeset(%Match{}, value).valid? == false
  end

  test "test changeset with invalid parameters" do
    value = %{
      id: 00003,
      league: "PS1",
      season: "201819",
      date: "15-04-2018",
      home: "home",
      away: "away",
      fthg: "WRONG",
      ftag: 0,
      ftr: "d",
      hthg: 0,
      htag: 0,
      htr: "d"
    }
    assert Match.changeset(%Match{}, value).valid? == false
  end

end
