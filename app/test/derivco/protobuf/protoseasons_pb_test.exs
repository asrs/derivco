defmodule ProtoSeasonTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoSeason.new" do
    test "testing with a parameter" do
      value = %ProtoSeason{
        league: ["SP1", "E0"],
        season: "201516"
      }

      ret = ProtoSeason.new(season: "201516", league: ["SP1", "E0"])

      assert ret == value
    end

    test "testing without a parameter" do
      value = %ProtoSeason{
        league: [],
        season: ""
      }

      assert ProtoSeason.new() == value
    end
  end

end

defmodule ProtoSeasonsProcessTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoSeasonsProcess.encode test" do

    test "first 50 character from list_matches" do
      value = "CggKBjIwMTUxNgoICgYyMDE2MTc="
      ret = ProtoSeasonsProcess.encode(Soccer.list_seasons
            |> DerivcoWeb.SeasonController.format)
            |> String.slice(0..50)

      assert ret == value
    end

    test "with none" do
      ret = ProtoSeasonsProcess.encode(%{})

      assert ret == ""
    end
  end

end
