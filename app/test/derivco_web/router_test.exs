defmodule DerivcoWeb.RouterTest do
  use DerivcoWeb.ConnCase

  test "404 Testing", %{conn: conn} do
    assert_error_sent :not_found, fn ->
      get(conn, "/this/is/error")
    end
  end

end
