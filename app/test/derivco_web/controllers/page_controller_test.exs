defmodule DerivcoWeb.PageControllerTest do
  use DerivcoWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "API REFERENCES"
  end
end
