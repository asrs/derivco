defmodule DerivcoWeb.ResultControllerTest do
  use DerivcoWeb.ConnCase

  describe "index" do
    test "lists all results with default format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/results")
      resp2 = get(conn, "/api/v1/result")

      resp3 = get(conn, "/api/v1/results?format=testtest")
      resp4 = get(conn, "/api/v1/result?format=testtest")

      assert resp1.status == 200
      assert resp2.status == 200
      assert resp3.status == 200
      assert resp3.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp3, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp4, :json) == "application/json; charset=utf-8"
    end

    test "lists all results with json format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/results?format=json")
      resp2 = get(conn, "/api/v1/result?format=json")

      assert resp1.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
    end

    test "lists all results with protobuf format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/results?format=protobuf")
      resp2 = get(conn, "/api/v1/result?format=protobuf")

      assert resp1.status == 200
      assert response_content_type(resp1, :proto) == "application/x-protobuf; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :proto) == "application/x-protobuf; charset=utf-8"
    end

    test "list with invalid parameters", %{conn: conn} do
      resp1 = get(conn, "/api/v1/results?footbal=tapas")
      resp2 = get(conn, "/api/v1/results?id=protobuf")

      assert resp1.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"

      assert resp2.status == 400
      assert json_response(resp2, 400) == "Bad Request"
    end
  end

end
