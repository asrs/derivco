defmodule DerivcoWeb.SeasonControllerTest do
  use DerivcoWeb.ConnCase

  describe "index" do
    test "lists all seasons with default format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/seasons")
      resp2 = get(conn, "/api/v1/season")

      resp3 = get(conn, "/api/v1/seasons?format=testtest")
      resp4 = get(conn, "/api/v1/season?format=testtest")

      assert resp1.status == 200
      assert resp2.status == 200
      assert resp3.status == 200
      assert resp3.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp3, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp4, :json) == "application/json; charset=utf-8"
    end

    test "lists all seasons with json format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/seasons?format=json")
      resp2 = get(conn, "/api/v1/season?format=json")

      assert resp1.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
    end

    test "lists all seasons with protobuf format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/seasons?format=protobuf")
      resp2 = get(conn, "/api/v1/season?format=protobuf")

      assert resp1.status == 200
      assert response_content_type(resp1, :proto) == "application/x-protobuf; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :proto) == "application/x-protobuf; charset=utf-8"
    end
  end

  describe "format" do
    test "is format work as attended with parameters ?" do
      p1 = [
        %{season: "201819", league: "sp1"},
        %{season: "201819", league: "e0"},
        %{season: "201819", league: "sp2"},
        %{season: "201819", league: "fr1"},
      ]
      value = [
        %{season: "201819",
          league: [
            "SP1",
            "E0",
            "SP2",
            "FR1",
          ]
        }
      ]
      ret = DerivcoWeb.SeasonController.format(p1)

      assert ret == value
    end

    test "is format work as attended with empty parameters ?" do
      p1 = []
      ret = DerivcoWeb.SeasonController.format(p1)

      assert ret == []
    end
  end

end
