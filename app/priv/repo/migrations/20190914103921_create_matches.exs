defmodule Derivco.Repo.Migrations.CreateMatches do
  use Ecto.Migration

  def change do
    create table(:matches) do
      add :league, :string
      add :season, :string
      add :date, :string
      add :home, :string
      add :away, :string
      add :fthg, :integer
      add :ftag, :integer
      add :ftr, :string
      add :hthg, :integer
      add :htag, :integer
      add :htr, :string

      timestamps()
    end

  end
end
