defmodule ProtoLeagues do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
    league: [ProtoLeague.t()]
  }
  defstruct [:league]

  field :league, 1, repeated: true, type: ProtoLeague
end

defmodule ProtoLeague do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
    league: String.t(),
    season: [ProtoSeasonsArray.t()]
  }
  defstruct [:league, :season]

  field :league, 1, type: :string
  field :season, 2, repeated: true, type: ProtoSeasonsArray
end

defmodule ProtoSeasonsArray do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
    season: String.t()
  }
  defstruct [:season]

  field :season, 1, type: :string
end

defmodule ProtoLeaguesProcess do

  @moduledoc """
  This module is complementary modules I made to separate the encoding logic
  from the controller, so all logic relative to Leagues protocol buffer is here.
  """

  @doc """
  encode/1 will encode a Enumerable data we retrieved for a query,
  it mean to be used with Soccer.list_leagues
  """

  # Encode take a enumarable objet,
  # we throw the raw argument to build during the building of
  # our protocol buffers struct.
  # then we pipe it to encode, and base64 encode so it ready
  # to go through the API render

  def encode(leagues) do
    ProtoLeagues.new(league: build(leagues))
    |> ProtoLeagues.encode
    |> Base.encode64
  end

  @doc """
  build, is a staging function that take the raw data parameter from ProtoLeaguesProcess.encode
  and make it ready to be use with ProtoLeagues.new
  """

  # Build, will loop through our element and create a struct
  # with the ProtoLeague.new function.
  # So it return an array of ProtoLeague struct ready to be
  # embed in ProtoLeagues struct
  # The tricks is in season: we loop through the seasons array to create
  # struct call ProtoSeasonsArray

  def build(leagues) do
    Enum.map(leagues, fn(e) ->
      ProtoLeague.new(
        league: e.league,
        season: Enum.map(e.season, fn(x) ->
          ProtoSeasonsArray.new(season: x)
        end)
      )
    end)
  end

end
