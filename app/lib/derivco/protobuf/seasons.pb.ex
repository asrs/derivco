defmodule ProtoSeasons do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          season: [ProtoSeason.t()]
        }
  defstruct [:season]

  field :season, 1, repeated: true, type: ProtoSeason
end

defmodule ProtoSeason do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          season: String.t(),
          league: [ProtoLeaguesArray.t()]
        }
  defstruct [:season, :league]

  field :season, 1, type: :string
  field :league, 2, repeated: true, type: ProtoLeaguesArray
end

defmodule ProtoLeaguesArray do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          season: String.t()
        }
  defstruct [:season]

  field :season, 1, type: :string
end

defmodule ProtoSeasonsProcess do

  @moduledoc """
  This module is complementary modules I made to separate the encoding logic
  from the controller, so all logic relative to Seasons protocol buffer is here.
  """

  @doc """
  encode/1 will encode a Enumerable data we retrieved for a query,
  it mean to be used with Soccer.list_seasons
  """

  # Encode take a enumarable objet,
  # we throw the raw argument to build during the building of
  # our protocol buffers struct.
  # then we pipe it to encode, and base64 encode so it ready
  # to go through the API render

  def encode(seasons) do
    ProtoSeasons.new(season: build(seasons))
    |> ProtoSeasons.encode
    |> Base.encode64
  end

  @doc """
  build, is a staging function that take the raw data parameter from ProtoSeasonsProcess.encode
  and make it ready to be use with ProtoSeasons.new
  """

  # Build, will loop through our element and create a struct
  # with the ProtoSeason.new function.
  # So it return an array of ProtoSeason struct ready to be
  # embed in ProtoSeasons struct
  # The tricks is in season: we loop through the leagues array to create
  # struct call ProtoLeaguesArray

  def build(seasons) do
    Enum.map(seasons, fn(e) ->
      ProtoSeason.new(
        season: e.season,
        leagues: Enum.map(e.league, fn(x) ->
          ProtoLeaguesArray.new(league: x)
        end)
      )
    end)
  end

end
