defmodule Derivco.Soccer.Match do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc """
  The Match ecto schema
  """

  schema "matches" do
    field :away, :string
    field :league, :string
    field :date, :string
    field :ftag, :integer
    field :fthg, :integer
    field :ftr, :string
    field :home, :string
    field :htag, :integer
    field :hthg, :integer
    field :htr, :string
    field :season, :string

    timestamps()
  end

  @doc false
  def changeset(match, attrs) do
    match
    |> cast(attrs, [:league, :season, :date, :home, :away, :fthg, :ftag, :ftr, :hthg, :htag, :htr])
    |> validate_required([:league, :season, :date, :home, :away, :fthg, :ftag, :ftr, :hthg, :htag, :htr])
  end
end
