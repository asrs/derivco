defmodule DerivcoWeb.MatchView do
  use DerivcoWeb, :view
  alias DerivcoWeb.MatchView

  @moduledoc false

  def render("index.json", %{matches: matches}) do
    %{data: render_many(matches, MatchView, "match.json")}
  end

  def render("match.json", %{match: match}) do
    %{id: match.id,
      league: String.upcase(match.league),
      season: match.season,
      date: match.date,
      home: String.capitalize(match.home),
      away: String.capitalize(match.away),
      fthg: match.fthg,
      ftag: match.ftag,
      ftr: String.upcase(match.ftr),
      hthg: match.hthg,
      htag: match.htag,
      htr: String.upcase(match.htr)}
  end
end
