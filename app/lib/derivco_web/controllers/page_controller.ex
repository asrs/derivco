defmodule DerivcoWeb.PageController do
  use DerivcoWeb, :controller

  @moduledoc """
  Phoenix generated controller module for render the landing page
  """

  @doc """
  A simple function that render the landing page
  """

  # This render function call the landing page on get "/"

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
