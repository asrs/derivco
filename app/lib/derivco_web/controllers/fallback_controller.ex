defmodule DerivcoWeb.FallbackController do
  use DerivcoWeb, :controller

  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.
  See `Phoenix.Controller.action_fallback/1` for more details.
  """

  @doc false
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(DerivcoWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  @doc false
  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(DerivcoWeb.ErrorView)
    |> render(:"404")
  end
end
