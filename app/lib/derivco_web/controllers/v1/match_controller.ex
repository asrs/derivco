defmodule DerivcoWeb.MatchController do
  use DerivcoWeb, :controller

  alias Derivco.Soccer

  action_fallback DerivcoWeb.FallbackController

  @moduledoc """
  This Module is the controller for our API endpoint api/v1/matches || api/v1/match
  """

  # Match module tend to be a general list of all data we have
  # It can get only one params, format, that let user choose the render
  # between JSON / Protobuf

  @doc """
  The index/2 function simply return all informations about Soccer matches we have in the database
  and launch the railroad/3 function so it render in the correct format
  """

  # format is retrieved by parameter, if it exist we use it,
  # if not we use the JSON format by default
  # In this function we simply call Soccer.list_matches to get elements.
  # Then we throw our query return to the function railroad for the render

  def index(conn, params) do
    format = params["format"] || "json"
    matches = Soccer.list_matches

    railroad(conn, format, matches)
  end

  @doc """
  This function will take the index/2 retrieved data and format variable to define
  how to render the data.
  """

  # This function will simply check format argument to render the correct data
  # It also encode Protobuf data before render it with text(conn, data)

  def railroad(conn, format, matches) do
    case format do
      "protobuf" -> text(conn |> put_resp_content_type("application/x-protobuf"),
          ProtoMatchesProcess.encode(matches))
     _ -> render(conn, "index.json", matches: matches)
    end
  end

end
