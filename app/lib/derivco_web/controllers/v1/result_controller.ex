defmodule DerivcoWeb.ResultController do
  use DerivcoWeb, :controller

  alias Derivco.Soccer
  alias Derivco.Soccer.Match

  plug :params_validity

  plug :filter, ~w(
    id leagues league
    seasons season home
    away ftr htr date fthg
    ftag hthg htag team) when action in [:index]

  action_fallback DerivcoWeb.FallbackController

  @moduledoc """
  This Module is the controller for our API endpoint api/v1/results || api/v1/result
  """

  # This module is mean to retrive Games/Matches result in a proper format
  # It's also take a lot of argument so we can search for a specific Games
  # The function filter is very important here, it's send filtered user parameters
  # to Soccer.composable_query, this context function from Soccer module
  # will build the query to retrieve data from the given arguments.

  @doc """
  This function let you get games results and informations from the database.
  It take parameters to build a custom SQL query with Soccer.list_results(params).
  It also format/1 the data in more readable way.

  Then launch the railroad/3 function so it render in the correct format
  """

  # Index will queries the elements that we want from the databases
  # it send previously stocked conn.assigns.filters to compose the query
  # and then pipe in format function before render the given data
  # Once the data is ready we launch our railroad function
  # to render the data in the correct format

  def index(conn, params) do
    format = params["format"] || "json"

    results = Match
              |> Soccer.list_results(conn.assigns.filters)
              |> format

    railroad(conn, format, results)
  end

  @doc """
  This function will take the index/2 retrieved data and format variable to define
  how to render the data.
  """

  # This function will simply check format argument to render the correct data
  # It also encode Protobuf data before render it with text(conn, data)

  def railroad(conn, format, results) do
    case format do
      "protobuf" -> text(conn |> put_resp_content_type("application/x-protobuf"),
          ProtoResultsProcess.encode(results))
     _ -> render(conn, "index.json", results: results)
    end
  end

  @doc """
  This function is simply call by Plug when index/2 is call, so it ignore bad parameter.
  It's stock the valids ones in conn.assigns
  """

  # This function is launch before we entered in index.
  # It will filter out all inexistant parameter that the user can provide
  # and then stock it in conn.assigns

  def filter(conn, params) do
    filters = Enum.filter(conn.params, fn({key, _value}) ->
      Enum.member?(params, key)
    end)
    conn |> assign(:filters, filters)
  end

  @doc """
  This is call by Plug when entering in any function of the controller,
  it will check for somes given users value,
  to avoid 500 error, so if a type is wrong it return a
  400 error.
  """

  # simply loop through the params and check the validity
  # if not return a 400 error

  def params_validity(conn, _) do
    valid = Enum.filter(conn.params, fn({key, value}) ->
      params_is_valid(key, value)
    end)

    if valid != [] do
      json(conn |> put_status(400), "Bad Request") |> halt()
    else
      conn
    end
  end

  # this function just check for some params that the given value is correct
  # if not it will help to throw a error 400

  defp params_is_valid(key, value) do
      case key do
        n when n in ["id", "fthg", "ftag", "hthg", "htag" ] ->
          if(Integer.parse(value) == :error, do: true, else: false)
        _ -> false
      end
  end

  @doc """
  This function will format the data in a more readable and User friendly manner
  """

  # This function will create a more readable data structure for the users
  # It start by enumarate all element we collected in data
  # and return a fine formatted Map

  def format(data) do
    Enum.map(data, fn(x) ->
      %{id: x.id,
        league: String.upcase(x.league),
        season: x.season,
        date: x.date,
        games: String.capitalize(x.home) <> " - " <> String.capitalize(x.away),
        winner: case x.ftr do
          "h" -> String.capitalize(x.home)
          "a" -> String.capitalize(x.away)
          _ -> "Drawn"
        end,
          results: Integer.to_string(x.fthg) <> " - " <> Integer.to_string(x.ftag)
        }
    end)
  end

end
