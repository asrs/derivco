#!/bin/sh

function gen_task() {
cat <<END > ./docker-config/release_task.ex
defmodule $2.ReleaseTask do
  @app :$1

  def migrate do
    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
END
}

function gen_entrypoint() {
cat <<END > ./docker-config/entrypoint.sh
#!/bin/sh

while ! pg_isready -q -h \$PGHOST -p \$PGPORT -U \$PGUSER
do
	echo "\$(date) - waiting for database to start"
	sleep 2
done

if [[ -z \`psql -Atqc "\\list \$PGDATABASE"\` ]]; then
	echo "Database \$PGDATABASE does not exist. Creating..."
	createdb -E UTF8 \$PGDATABASE -l en_US.UTF-8 -T template0
	/app/bin/$1 eval "$2.ReleaseTask.migrate"
else
	echo "Database \$PGDATABASE does exists. Continues..."
fi

#export RELEASE_NODE=$(cat /etc/hostname)

/app/bin/$1 start
END
}

function gen_releases() {
cat <<END > ./docker-config/releases.exs
import Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :$1, $2Web.Endpoint,
  server: true,
  http: [port: System.get_env("PORT") || 4000],
  url: [host: System.get_env("HOST") || "localhost", port: System.get_env("PORT") || "4000"],
  cache_static_manifest: "priv/static/cache_manifest.json"

config :$1, $2.Repo,
  # ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")
END
}

function gen_rel(){
cat <<END > ./docker-config/rel.exs
use Mix.Config

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :$1, $2Web.Endpoint,
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
  secret_key_base: secret_key_base

config :logger, level: :info
END
}

function gen() {
	mkdir -p docker-config
	gen_task $1 $2;
	gen_rel $1 $2;
	gen_releases $1 $2;
	gen_entrypoint $1 $2;
	chmod +x ./docker-config/entrypoint.sh
}

if [ $# -eq 0 ]
  then
	echo "usage: gen_docker.sh arg1 arg2"
fi

if [ -z "$1" ]
  then
	echo -e "  - arg1: Please provide your app name in snake case, example -> my_portfolio"
elif [ -z "$2" ]
  then
	echo -e "  - arg2: Please provide your app name in pascal case, example -> MyPortfolio"
else
	gen "$1" "$2"
fi

exit
