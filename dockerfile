#==============================================================================#
#------------------------------------------------------------------------------#
# BUILD IMAGES

FROM elixir:alpine as builder

ARG secret

RUN apk --update add git inotify-tools curl wget nodejs npm \
&& rm -rf /var/cache/apk/*

RUN mkdir -p /build
COPY ./app /build

WORKDIR /build
RUN rm -rf ./_build
RUN rm -rf ./deps

COPY ./docker-config/release_task.ex /build/lib/release_task.ex
COPY ./docker-config/releases.exs /build/config/releases.exs
COPY ./docker-config/rel.exs /build/config/rel.exs

ENV SECRET_KEY_BASE="$secret"

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix archive.install hex phx_new --force

RUN mix do deps.get, deps.compile, release.init

RUN npm install --prefix ./assets
RUN npm rebuild --prefix ./assets
RUN npm run deploy --prefix ./assets

RUN mix phx.digest

RUN mix compile
RUN MIX_ENV=rel mix compile
RUN MIX_ENV=rel mix release --overwrite

#==============================================================================#
#------------------------------------------------------------------------------#
# RELEASE IMAGES

FROM elixir:alpine as app

ARG app_name

RUN apk --update add bash openssl postgresql-client \
&& rm -rf /var/cache/apk/*

RUN mkdir -p /app && mkdir -p priv && mkdir -p priv/repo
WORKDIR /app

COPY --from=builder /build/_build/rel/rel/"$app_name" ./
COPY --from=builder /build/priv/repo/Data.csv ./priv/repo/
COPY ./docker-config/entrypoint.sh /app

CMD ["/app/entrypoint.sh"]
